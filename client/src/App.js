import React from 'react';

import TrailSearcher from './components/TrailSearcher';
import TrailStatusOverview from './components/TrailStatusOverview';

import './App.css';

// TODO: get trail location programmatically
// TODO: search for trails

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ridelist: []
    };
  }

  render() {
    return (
      <div className="main-layout">
        <div className="label0 label-box label-box--light">
          <span>SEARCH</span>
        </div>
        <div className="search item-container">
          <TrailSearcher
            ridelist={JSON.parse(JSON.stringify(this.state.ridelist))}
            updateRidelist={ridelist => this.setState({ridelist})}
          />
        </div>
        <div className="label1 label-box label-box--dark">
          <span>?????</span>
        </div>
        <div>
          <TrailStatusOverview ridelist={this.state.ridelist}/>
        </div>
      </div>
    );
  }
}

export default App;
