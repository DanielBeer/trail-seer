import React from 'react';
import ReactLoading from 'react-loading';

function LoadingSpinner() {
  return (
    <div style={{display: 'flex', justifyContent: 'right', position: 'absolute', width: '100%', height: '100%', top: 0, right: 0}}>
      <ReactLoading type={'spinningBubbles'} color={'gray'}/>
    </div>
  );
}

export default LoadingSpinner;
