import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import HighchartsPatternFill from "highcharts-pattern-fill";
import LoadingSpinner from './LoadingSpinner';

HighchartsPatternFill(Highcharts);

// TODO: no reports in one year
// TODO: remove brief changes in status (e.g. major for falling tree blocking trail)
// TODO: limit number of years shown at a time
// TODO: add a legend for chart colours
// TODO: slider to adjust estimated accuracy of reports (how far to extend gradient)
// TODO: remove or customize tooltip (show number of reports?)

const STATUS_COLOURS_RED = 'rgb(255, 53, 53)';
const STATUS_COLOURS_ORANGE = 'rgb(255, 140, 53)';
const STATUS_COLOURS_GREEN = 'rgb(55, 183, 0)';

const STATUS_COLOURS_RED_BG = 'rgba(255, 53, 53, 0.3)';
const STATUS_COLOURS_ORANGE_BG = 'rgba(255, 140, 53, 0.3)';
const STATUS_COLOURS_GREEN_BG = 'rgba(55, 183, 0, 0.3)';

const STATUS_COLOURS_TRANSPARENT = 'rgba(0, 0, 0, 0)';

const STATUS_POOR = 0.4;
const STATUS_FAIR = 0.8;

const MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
const FULL_MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

/*const CONDITIONS_COLOURS = {
  Unknown: 'pink',

  'Snow Groomed': 'lightblue',
  'Snow Packed': 'lightblue',
  'Snow Covered': 'lightblue',
  Icy: 'lightblue',

  'Freeze/thaw Cycle': 'rgb(201, 164, 94)',
  'Prevalent Mud': 'rgb(201, 164, 94)',
  Wet: 'rgb(201, 164, 94)',

  Variable: 'lightgreen',
  Ideal: 'lightgreen',
  Dry: 'lightgreen',
  'Very Dry': 'lightgreen'
};*/

class SeasonalStatusChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingData: true
    };

    this.charts = {};

    this.updateData = this.updateData.bind(this);
  }

  async updateData() {
    this.setState({loadingData: true});

    const res = await fetch(`http://localhost:9000/trail/reports/${this.props.trailID}`);
    let reports = Object.values(await res.json());
    reports.forEach(report => report.date = new Date(report.date));

    for (let year = reports[0].date.getFullYear(); year <= new Date().getFullYear(); ++year) {
      this.charts[year] = React.createRef();
    }

    let reportsByYear = {};

    let currentYear = reports[0].date.getFullYear();

    let reportsForYear = [];

    for (let month = 0; month < 12; ++month) {
      reportsForYear.push([]);
    }

    for (let report of reports) {
      const year = report.date.getFullYear();
      const month = report.date.getMonth();

      if (year > currentYear) {
        reportsByYear[currentYear] = reportsForYear;

        reportsForYear = [];

        for (let month = 0; month < 12; ++month) {
          reportsForYear.push([]);
        }

        currentYear = year;
      }

      reportsForYear[month].push(report);
    }

    reportsByYear[currentYear] = reportsForYear;

    let scoresByYear = {};

    let lastReport;

    for (let year in reportsByYear) {
      let reports = reportsByYear[year];

      let scoresForYear = [];

      for (let month = 0; month < 12; ++month) {
        let reportsForMonth = reports[month];

        if ((!lastReport && reportsForMonth.length === 0) || new Date(year, month, 1).getTime() > Date.now()) {
          scoresForYear.push(-1);
        } else if (lastReport && reportsForMonth.length === 0) {
          scoresForYear.push(parseInt(lastReport.score) / 2);
        } else if (!lastReport && reportsForMonth.length > 0) {
          let scoreForMonth = 0;

          const startDate = reportsForMonth[0].date;

          lastReport = reportsForMonth[0];

          for (let i = 1; i < reportsForMonth.length; ++i) {
            scoreForMonth += parseInt(lastReport.score) * (reportsForMonth[i].date - lastReport.date);
            lastReport = reportsForMonth[i];
          }

          const endDate = new Date(year, month + 1, 0).getTime();

          scoreForMonth += parseInt(lastReport.score) * (endDate - lastReport.date);

          scoresForYear.push(scoreForMonth / (endDate - startDate) / 2);
        } else {
          let scoreForMonth = 0;

          const startDate = new Date(year, month, 1).getTime();

          scoreForMonth += parseInt(lastReport.score) * (reportsForMonth[0].date - startDate);
        
          lastReport = reportsForMonth[0];

          for (let i = 1; i < reportsForMonth.length; ++i) {
            scoreForMonth += parseInt(lastReport.score) * (reportsForMonth[i].date - lastReport.date);
            lastReport = reportsForMonth[i];
          }

          const endDate = new Date(year, month + 1, 0).getTime();

          scoreForMonth += parseInt(lastReport.score) * (endDate - lastReport.date);

          scoresForYear.push(scoreForMonth / (endDate - startDate) / 2);
        }
      }

      scoresByYear[year] = scoresForYear;
    }

    let chartColoursByYear = {};
    
    for (let year in scoresByYear) {
      chartColoursByYear[year] = scoresByYear[year].map(scoreForMonth => {
        if (scoreForMonth === -1) {
          return STATUS_COLOURS_TRANSPARENT;
        } else if (scoreForMonth < STATUS_POOR) {
          return STATUS_COLOURS_RED;
        } else if (scoreForMonth < STATUS_FAIR) {
          return STATUS_COLOURS_ORANGE;
        } else {
          return STATUS_COLOURS_GREEN;
        }
      });
    }

    let avgScoresByMonth = [];

    for (let month = 0; month < 12; ++month) {
      let avgScoreForMonth = 0;
      let numScores = 0;

      for (let scoresForYear of Object.values(scoresByYear)) {
        let scoreForMonth = scoresForYear[month];

        if (scoreForMonth !== -1) {
          avgScoreForMonth += scoreForMonth;
          ++numScores;
        }
      }

      if (numScores > 0) {
        avgScoresByMonth.push(avgScoreForMonth / numScores);
      } else {
        avgScoresByMonth.push(-1);
      }
    }

    this.setState({reportsByYear, chartColoursByYear, scoresByYear, avgScoresByMonth, loadingData: false});
  }

  async componentDidMount() {
    await this.updateData();
  }

  async componentDidUpdate(props) {
    if (this.props.trailID !== props.trailID) {
      await this.updateData();
    }
  }

  render() {
    if (!this.state || this.state.loadingData) {
      return <LoadingSpinner/>;
    }

    let plotBands = [];

    for (let month = 0; month < 12; ++month) {
      let avgScoreForMonth = this.state.avgScoresByMonth[month];

      if (avgScoreForMonth === -1) {
        continue;
      }

      let color;

      if (avgScoreForMonth < STATUS_POOR) {
        color = STATUS_COLOURS_RED_BG;
      } else if (avgScoreForMonth < STATUS_FAIR) {
        color = STATUS_COLOURS_ORANGE_BG;
      } else {
        color = STATUS_COLOURS_GREEN_BG;
      }

      plotBands.push({
        color,
        from: month,
        to: month + 1,
        id: month
      });
    }

    const chartHeight = Math.floor((400 - 18) / Object.keys(this.state.chartColoursByYear).length);

    return (
      <div>
        {Object.keys(this.state.chartColoursByYear).map(year => {
          let chartColoursForYear = this.state.chartColoursByYear[year];

          let series = [];

          let width = 0;

          for (let month = 11; month >= 0; --month) {
            if (chartColoursForYear[month] === STATUS_COLOURS_TRANSPARENT) {
              ++width;
            }

            if (width === 0 || month === 0) {
              series.push({
                color: chartColoursForYear[month],
                data: [month === 0 && width > 0 ? width : 1],
                showInLegend: false,
                enableMouseTracking: width === 0,
                states: {
                  hover: {
                    enabled: width === 0
                  }
                },
                name: month
              });
            }
          }

          const reportsByYear = this.state.reportsByYear;
          const scoresByYear = this.state.scoresByYear;
          const avgScoresByMonth = this.state.avgScoresByMonth;

          return <HighchartsReact
            key={year}
            ref={this.charts[year]}
            highcharts={Highcharts}
            options={{
              chart: {
                type: 'bar',
                height: `${parseInt(year) === new Date().getFullYear() ? chartHeight + 18 : chartHeight}px`,
                spacingTop: 0,
                spacingBottom: 0
              },
              credits: {
                enabled: false
              },
              title: {
                text: ''
              },
              tooltip: {
                borderColor: 'gray',
                outside: true,
                formatter: function() {
                  const month = this.series.name;

                  let overall;

                  if (avgScoresByMonth[month] < STATUS_POOR) {
                    overall = 'Closed/Poor';
                  } else if (avgScoresByMonth[month] < STATUS_FAIR) {
                    overall = 'Fair';
                  } else {
                    overall = 'Great';
                  }

                  let text = `<strong>${FULL_MONTHS[month]}: ${overall} conditions</strong><br>`;

                  for (let year2 in reportsByYear) {
                    if (scoresByYear[year2][month] === -1) {
                      continue;
                    }

                    const io = year2 === year ? '<i>' : '';
                    const ic = year2 === year ? '</i>' : '';

                    let conditions;

                    if (scoresByYear[year2][month] < STATUS_POOR) {
                      conditions = 'Closed/Poor';
                    } else if (scoresByYear[year2][month] < STATUS_FAIR) {
                      conditions = 'Fair';
                    } else {
                      conditions = 'Great';
                    }

                    const numReports = reportsByYear[year2][month].length;
                    const s = numReports === 1 ? '' : 's';
                    const br = year2 === new Date().getFullYear() ? '' : '<br>';

                    text += `${io}${year2}: ${conditions} conditions based on ${numReports} report${s}.${ic}${br}`; 
                  }
                  
                  return text;
                }
              },
              plotOptions: {
                series: {
                  stacking: 'normal',
                  borderColor: 'transparent',
                  borderWidth: 0,
                  point: {
                    events: {
                      mouseOver: (evt) => { 
                        for (let year2 in this.charts) {
                          let chartRef = this.charts[year2];

                          if (chartRef.current) {
                            let chart = chartRef.current.chart;
                            let targetSeriesName = evt.target.series.name;

                            if (year2 !== year) {                              
                              for (let series of chart.series) {
                                if (series.name === targetSeriesName) {
                                  series.points[0].setState('hover');
                                } else {
                                  series.points[0].setState('inactive');
                                }
                              }
                            }

                            for (let band of chart.yAxis[0].plotLinesAndBands) {
                              if (band.id === targetSeriesName) {
                                let newBand = Object.assign({}, band.options);
                                newBand.color = newBand.color.replace('0.3', '0.5');

                                chart.yAxis[0].removePlotBand(band.id);
                                chart.yAxis[0].addPlotBand(newBand);
                              }
                            }
                          }
                        }
                      },
                      mouseOut: () => {
                        for (let chartRef of Object.values(this.charts)) {
                          if (chartRef.current) {
                            let chart = chartRef.current.chart;

                            for (let series of chart.series) {
                              series.points[0].setState('normal');
                            }

                            for (let band of chart.yAxis[0].plotLinesAndBands) {
                              if (band.options.color.includes('0.5')) {
                                let newBand = Object.assign({}, band.options);
                                newBand.color = newBand.color.replace('0.5', '0.3');

                                chart.yAxis[0].removePlotBand(band.id);
                                chart.yAxis[0].addPlotBand(newBand);
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              },
              xAxis: {
                categories: [year]
              },
              yAxis: {
                title: {
                  text: ''
                },
                labels: {
                  formatter: (y) => MONTHS[y.value],
                  enabled: parseInt(year) === new Date().getFullYear()
                },
                max: 12,
                plotBands
              },
              series
            }}
          />
        })}
      </div>
    );
  }
}

export default SeasonalStatusChart;
