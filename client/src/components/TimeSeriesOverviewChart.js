import React from 'react';
import Highcharts from 'highcharts/highstock';
import HighchartsReact from 'highcharts-react-official';
import HighchartsMore from 'highcharts/highcharts-more';
import LoadingSpinner from './LoadingSpinner';

HighchartsMore(Highcharts);

// TODO: plot line for 0 degrees C
// TODO: legend

const STATUS_COLOURS = ['rgba(55, 183, 0, 0.3)', 'rgba(255, 221, 53, 0.3)', 'rgba(255, 140, 53, 0.3)', 'rgba(255, 53, 53, 0.3)'];

class TimeSeriesOverviewChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      trailReports: null,
      weather: null
    };
  }

  async componentDidMount() {
    const locationRes = await fetch(`http://localhost:9000/trails/location/${this.props.trailID}`);
    const locationJSON = await locationRes.json();

    const d = new Date();
    const today = `${d.getFullYear()}-`
      + `${d.getMonth() < 10 ? '0' + d.getMonth() : d.getMonth()}-`
      + `${d.getDay() < 10 ? '0' + d.getDay() : d.getDay()}`;
    const todayOneYearBefore = `${d.getFullYear() - 1}-`
      + `${d.getMonth() < 10 ? '0' + d.getMonth() : d.getMonth()}-`
      + `${d.getDay() < 10 ? '0' + d.getDay() : d.getDay()}`;

    const weatherRes = await fetch(`http://localhost:9000/weather/${locationJSON.lat}/${locationJSON.lon}/${todayOneYearBefore}/${today}`);
    const weatherJSON = await weatherRes.json();

    const trailReportsRes = await fetch(`http://localhost:9000/trails/reports/${this.props.trailID}`);
    const trailReportsJSON = await trailReportsRes.json();

    this.setState({trailReports: trailReportsJSON.reports, weather: weatherJSON.weather});
  }

  render() {
    if (this.state === null || this.state.trailReports === null || this.state.weather === null) {
      return <LoadingSpinner/>;
    }

    const reports = this.state.trailReports;
    const yearAgo = Date.now() - 1000 * 60 * 60 * 24 * 7 * 52;

    let trailReportsForYear = [];

    for (let i = reports.length - 1; i >= 0; --i) {
      trailReportsForYear.push(reports[i]);

      if (new Date(reports[i]).getTime() <= yearAgo) break;
    }

    trailReportsForYear.reverse();

    let plotBands = [{
      color: STATUS_COLOURS[reports[0].status.id - 1],
      from: new Date(reports[0].date).getTime(),
      to: new Date(reports[1].date).getTime()
    }];
    let status = reports[0].status.id;

    for (let i = 0; i < trailReportsForYear.length - 1; ++i) {
      const nextDate = i < trailReportsForYear.length - 1 ? new Date(reports[i + 1].date).getTime() : Date.now();

      if (status === reports[i].status.id) {
        plotBands[plotBands.length - 1].to = nextDate;
      } else {
        plotBands.push({
          color: STATUS_COLOURS[reports[i].status.id - 1],
          from: new Date(reports[i].date).getTime(),
          to: nextDate
        });
        status = reports[i].status.id;
      }
    }

    return (
      <HighchartsReact
        highcharts={Highcharts}
        constructorType={'stockChart'}
        options={{
          chart: {
            type: 'area',
            backgroundColor: 'transparent'
          },
          credits: {
            enabled: false
          },
          legend: {
            enabled: true
          },
          xAxis: {
            type: 'datetime',
            plotBands: plotBands
          },
          yAxis: [
            {
              title: {
                text: 'Temperature'
              },
              labels: {
                format: '{value}°C'
              },
              min: Math.floor(Math.min(...this.state.weather.map(day => day.tavg)) / 10) * 10,
              max: Math.ceil(Math.max(...this.state.weather.map(day => day.tavg)) / 10) * 10,
              opposite: false
            },
            {
              title: {
                text: 'Precipitation'
              },
              labels: {
                format: '{value}mm',
              }
            }
          ],
          plotOptions: {
            column: {
              stacking: 'normal'
            }
          },
          series: [
            {
              name: 'Snow',
              yAxis: 1,
              data: this.state.weather.map(day => [new Date(day.date).getTime(), day.snow || 0]),
              color: 'url(#highcharts-default-pattern-1)'
            },
            {
              name: 'Precipitation',
              yAxis: 1,
              data: this.state.weather.map(day => [new Date(day.date).getTime(), day.prcp || 0]),
              color: 'url(#highcharts-default-pattern-0)',//'lightblue',
              tooltip: {
                valueSuffix: 'mm'
              }
            },
            {
              name: 'Temperature',
              type: 'spline',
              data: this.state.weather.map(day => {
                return {x: new Date(day.date).getTime(), y: day.tavg};
              }),
              color: 'red',
              tooltip: {
                valueSuffix: '°C'
              },
              marker: {
                symbol: 'circle'
              },
            }
          ]
        }}
      />
    );
  }
}

export default TimeSeriesOverviewChart;
