import React from 'react';
import Highcharts from 'highcharts';
import more from 'highcharts/highcharts-more';
import HighchartsReact from 'highcharts-react-official';
import HighchartsPatternFill from "highcharts-pattern-fill";

more(Highcharts);
HighchartsPatternFill(Highcharts);

Highcharts.seriesTypes.columnrange.prototype.drawDataLabels = Highcharts.seriesTypes.column.prototype.drawDataLabels;

const STATUS_COLOURS_POOR = 'rgb(255, 53, 53)';
const STATUS_COLOURS_FAIR = 'rgb(255, 140, 53)';
const STATUS_COLOURS_IDEAL = 'rgb(55, 183, 0)';

const STATUS_POOR = 0.4;
const STATUS_FAIR = 0.8;

const MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

class TrailStatusOverview extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      chartColoursByTrail: [],
    }
  }

  async componentDidUpdate(prevProps) {
    if (this.props.ridelist.length === prevProps.ridelist.length) {
      return;
    }

    let chartColoursByTrail = [];

    for (let trail of this.props.ridelist) {
      const res = await fetch(`http://localhost:9000/trails/${trail.id}/reports`);
      let reports = Object.values(await res.json());
      reports.forEach(report => report.date = new Date(report.date));

      let reportsByYear = {};

      let currentYear = reports[0].date.getFullYear();
  
      let reportsForYear = [];
  
      for (let month = 0; month < 12; ++month) {
        reportsForYear.push([]);
      }
  
      for (let report of reports) {
        const year = report.date.getFullYear();
        const month = report.date.getMonth();
  
        if (year > currentYear) {
          reportsByYear[currentYear] = reportsForYear;
  
          reportsForYear = [];
  
          for (let month = 0; month < 12; ++month) {
            reportsForYear.push([]);
          }
  
          currentYear = year;
        }
  
        reportsForYear[month].push(report);
      }
  
      reportsByYear[currentYear] = reportsForYear;
  
      let scoresByYear = {};
  
      let lastReport;
  
      for (let year in reportsByYear) {
        let reports = reportsByYear[year];
  
        let scoresForYear = [];
  
        for (let month = 0; month < 12; ++month) {
          let reportsForMonth = reports[month];
  
          if ((!lastReport && reportsForMonth.length === 0) || new Date(year, month, 1).getTime() > Date.now()) {
            scoresForYear.push(-1);
          } else if (lastReport && reportsForMonth.length === 0) {
            scoresForYear.push(parseInt(lastReport.score) / 2);
          } else if (!lastReport && reportsForMonth.length > 0) {
            let scoreForMonth = 0;
  
            const startDate = reportsForMonth[0].date;
  
            lastReport = reportsForMonth[0];
  
            for (let i = 1; i < reportsForMonth.length; ++i) {
              scoreForMonth += parseInt(lastReport.score) * (reportsForMonth[i].date - lastReport.date);
              lastReport = reportsForMonth[i];
            }
  
            const endDate = new Date(year, month + 1, 0).getTime();
  
            scoreForMonth += parseInt(lastReport.score) * (endDate - lastReport.date);
  
            scoresForYear.push(scoreForMonth / (endDate - startDate) / 2);
          } else {
            let scoreForMonth = 0;
  
            const startDate = new Date(year, month, 1).getTime();
  
            scoreForMonth += parseInt(lastReport.score) * (reportsForMonth[0].date - startDate);
          
            lastReport = reportsForMonth[0];
  
            for (let i = 1; i < reportsForMonth.length; ++i) {
              scoreForMonth += parseInt(lastReport.score) * (reportsForMonth[i].date - lastReport.date);
              lastReport = reportsForMonth[i];
            }
  
            const endDate = new Date(year, month + 1, 0).getTime();
  
            scoreForMonth += parseInt(lastReport.score) * (endDate - lastReport.date);
  
            scoresForYear.push(scoreForMonth / (endDate - startDate) / 2);
          }
        }
  
        scoresByYear[year] = scoresForYear;
      }

      let scoresByMonth = [];

      for (let month = 0; month < 12; ++month) {
        let scoreForMonth = 0;
        let totalReports = 0;

        for (let year in scoresByYear) {
          const score = scoresByYear[year][month];
          const numReports = reportsByYear[year][month].length + 1;

          if (score !== -1) {
            scoreForMonth += score * numReports;
            totalReports += numReports;
          }
        }

        if (totalReports === 0) {
          scoresByMonth.push(-1);
        } else {
          scoresByMonth.push(scoreForMonth / totalReports);
        }
      }

      chartColoursByTrail.push(scoresByMonth.map(scoreForMonth => {
        if (scoreForMonth === -1) {
          return -1;
        } else if (scoreForMonth < STATUS_POOR) {
          return STATUS_COLOURS_POOR;
        } else if (scoreForMonth < STATUS_FAIR) {
          return STATUS_COLOURS_FAIR;
        } else {
          return STATUS_COLOURS_IDEAL;
        }
      }));
    }

    this.setState({chartColoursByTrail});
  }

  render() {
    const chartColoursByTrail = this.state.chartColoursByTrail;

    let chartColoursByTrailMerged = [];
    let trailLabels = [];

    if (this.props.ridelist.length > 0 && chartColoursByTrail.length === this.props.ridelist.length) {
      chartColoursByTrailMerged.push(chartColoursByTrail[0]);
      trailLabels.push(this.props.ridelist[0].name);

      for (let i = 1; i < chartColoursByTrail.length; ++i) {
        let merged = false;

        for (let j = 0; j < chartColoursByTrailMerged.length; ++j) {
          let matching = true;

          for (let month = 0; month < 12; ++month) {
            if (chartColoursByTrail[i][month] !== chartColoursByTrailMerged[j][month]) {
              matching = false;
              break;
            }
          }

          if (matching) {
            trailLabels[j] += ', ' + this.props.ridelist[i].name;
            merged = true;
            break;
          }
        }

        if (!merged) {
          chartColoursByTrailMerged.push(chartColoursByTrail[i]);
          trailLabels.push(this.props.ridelist[i].name);
        }
      }
    }

    let data = [];

    for (let i = 0; i < chartColoursByTrailMerged.length; ++i) {
      const chartColours = chartColoursByTrailMerged[i];

      let low = 0;
      let stops = [];

      for (let i = 0; i < 12; ++i) {
        let chartColour = chartColours[i];

        if (chartColour === -1) {
          low = i + 1;
        } else {
          stops.push([i / (12 - low), chartColour]);
          stops.push([(i + 1) / (12 - low), chartColour]);
        }
      }

      data.push({
        name: trailLabels[i],
        low,
        high: 12,
        color: {
          linearGradient: {x1: 0, x2: 0, y1: 1, y2: 0},
          stops,
        }
      });
    }

    return <HighchartsReact
      highcharts={Highcharts}
      options={{
        chart: {
          type: 'columnrange',
          inverted: true,
        },
        credits: {
          enabled: false,
        },
        title: {
          text: '',
        },
        xAxis: {
          categories: trailLabels,
          labels: {
            enabled: false,
          },
        },
        yAxis: {
          min: 0,
          max: 12,
          tickPositions: [0, 0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12],
          gridLineColor: 'transparent',
          minorTickInterval: 1,
          labels: {
            formatter: function() {
              return MONTHS[this.pos - 0.5];
            },
            rotation: 0,
          },
          title: '',
        },
        plotOptions: {
          columnrange: {
            dataLabels: {
              inside: true,
              enabled: true,
              formatter: function() {
                return this.x;
              },
            }
          }
        },
        series: [{
          data,
          showInLegend: false,
        }],
      }}
    />
  }
}

export default TrailStatusOverview;
