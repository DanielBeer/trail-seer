import React from 'react';
import {
  Paper,
  TextField,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableContainer,
  IconButton,
  Tooltip,
} from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles';
import {
  AddCircleOutline,
  Cancel,
  Search,
} from '@material-ui/icons';

import './TrailSearcher.css';

// TODO: automatically scroll table to top after a new search is performed
// TODO: search when enter pressed

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    padding: '0.3em',
  },
  body: {
    fontSize: 14,
    padding: 0,
    paddingLeft: '0.3em',
  }
}))(TableCell);

class TrailSearcher extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      trailQuery: '',
      regionQuery: '',
      trails: [],
      loadingTrails: false,
      searchResults: [],
      searchResultIDs: new Set(),
      ridelistIDs: new Set(),
    };

    this.search = this.search.bind(this);

    this.addToRidelist = this.addToRidelist.bind(this);
    this.removeFromRidelist = this.removeFromRidelist.bind(this);

    this.addAllToRidelist = this.addAllToRidelist.bind(this);
    this.removeAllFromRidelist = this.removeAllFromRidelist.bind(this);

    this.tableRef = React.createRef();
  }

  async search() {
    this.setState({loadingTrails: true});

    const trailQuery = this.state.trailQuery.trim();
    const regionQuery = this.state.regionQuery.trim();

    let filter = {};

    if (trailQuery) {
      filter.name = trailQuery;
    }

    if (regionQuery) {
      filter.region = regionQuery;
    }

    const filterRes = await fetch(`http://localhost:9000/filters`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(filter)
    });

    const filterID = filterRes.headers.get('location').split('/')[2];

    const trailsRes = await fetch(`http://localhost:9000/trails?filterId=${filterID}`);
    const trails = Object.values(await trailsRes.json());

    let searchResults = [];
    let searchResultIDs = new Set();

    for (let trail of trails) {
      searchResults.push(Object.assign(trail, {showInTable: !this.state.ridelistIDs.has(trail.id)}));
      searchResultIDs.add(trail.id);
    }

    this.setState({trails, searchResults, searchResultIDs, loadingTrails: false});
  }

  addToRidelist(trail) {
    this.props.ridelist.push(trail);
    this.state.ridelistIDs.add(trail.id);

    for (let searchTrail of this.state.searchResults) {
      if (searchTrail.id === trail.id) {
        searchTrail.showInTable = false;
        break;
      }
    }

    this.props.updateRidelist(this.props.ridelist);
    this.setState({});
  }

  removeFromRidelist(trail) {
    const idx = this.props.ridelist
      .map(ridelistTrail => ridelistTrail.id)
      .indexOf(trail.id);

    this.props.ridelist.splice(idx, 1);
    this.state.ridelistIDs.delete(trail.id);

    if (this.state.searchResultIDs.has(trail.id)) {
      for (let searchTrail of this.state.searchResults) {
        if (searchTrail.id === trail.id) {
          searchTrail.showInTable = true;
          break;
        }
      }
    }

    this.props.updateRidelist(this.props.ridelist);
    this.setState({});
  }

  addAllToRidelist() {
    for (let trail of this.state.trails) {
      this.props.ridelist.push(trail);
      this.state.ridelistIDs.add(trail.id);
    }

    for (let trail of this.state.searchResults) {
      trail.showInTable = false;
    }

    this.props.updateRidelist(this.props.ridelist);
    this.setState({});
  }

  removeAllFromRidelist() {
    for (let searchTrail of this.state.searchResults) {
      if (this.state.ridelistIDs.has(searchTrail.id)) {
        searchTrail.showInTable = true;
      }
    }

    this.state.ridelistIDs.clear();

    this.props.updateRidelist([]);
    this.setState({});
  }

  render() {
    return (
      <div className="search-container">
        <div className="search-box">
          <div className="filter-box">
            <div className="filter-items">
              <TextField label="Search by trail name" variant="outlined" size="small" onChange={evt => this.setState({trailQuery: evt.target.value})}/>
              <TextField style={{marginTop: '0.7em'}} label="Search by region name" variant="outlined" size="small" onChange={evt => this.setState({regionQuery: evt.target.value})}/>
            </div>
            <IconButton disabled={this.state.trailQuery.trim() === '' && this.state.regionQuery.trim() === ''} onClick={this.search}>
              <Search/>
            </IconButton>
          </div>
          <div className="results-box">
            <TableContainer component={Paper} className="table">
              <Table stickyHeader ref={this.tableRef}>
                <colgroup>
                  <col style={{width: '55%'}}/>
                  <col style={{width: '40%'}}/>
                  <col style={{width: '5%'}}/>
                </colgroup>
                <TableHead>
                  <TableRow>
                    <StyledTableCell>Trail</StyledTableCell>
                    <StyledTableCell>Region</StyledTableCell>
                    <StyledTableCell>
                      <Tooltip title="Add all to Ridelist" onClick={() => this.addAllToRidelist()}>
                        <IconButton aria-label="add">
                          <AddCircleOutline style={{fill: 'white'}}/>
                        </IconButton>
                      </Tooltip>
                    </StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.searchResults.filter(trail => trail.showInTable).map(trail => (
                    <TableRow key={trail.id}>
                      <StyledTableCell>{trail.name}</StyledTableCell>
                      <StyledTableCell><i>{trail.region}</i></StyledTableCell>
                      <StyledTableCell>
                        <Tooltip title="Add to Ridelist" onClick={() => this.addToRidelist(trail)}>
                          <IconButton aria-label="add">
                            <AddCircleOutline/>
                          </IconButton>
                        </Tooltip>
                      </StyledTableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </div>
        </div>
        <div className="ridelist-box">
          <center>
            <h3>Ridelist</h3>
          </center>
          <TableContainer className="table" component={Paper}>
            <Table stickyHeader ref={this.tableRef}>
              <colgroup>
                <col style={{width: '55%'}}/>
                <col style={{width: '40%'}}/>
                <col style={{width: '5%'}}/>
              </colgroup>
              <TableHead>
                <TableRow>
                  <StyledTableCell>Trail</StyledTableCell>
                  <StyledTableCell>Region</StyledTableCell>
                  <StyledTableCell>
                    <Tooltip title="Remove all from Ridelist" onClick={() => this.removeAllFromRidelist()}>
                      <IconButton aria-label="remove">
                        <Cancel style={{fill: 'white'}}/>
                      </IconButton>
                    </Tooltip>
                  </StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.props.ridelist.map(trail => (
                  <TableRow key={trail.id}>
                    <StyledTableCell>{trail.name}</StyledTableCell>
                    <StyledTableCell>{trail.region}</StyledTableCell>
                    <StyledTableCell>
                      <Tooltip title="Remove from Ridelist" onClick={() => this.removeFromRidelist(trail)}>
                        <IconButton aria-label="remove">
                          <Cancel/>
                        </IconButton>
                      </Tooltip>
                    </StyledTableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </div>
    );
  }
}

export default TrailSearcher;
