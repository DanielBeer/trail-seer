import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import HighchartsMore from 'highcharts/highcharts-more';
import LoadingSpinner from './LoadingSpinner';

HighchartsMore(Highcharts);

// TODO: disable tooltip for temperature range
// TODO: round edges of temperature

class ClimateOverviewChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      climate: null
    };
  }

  async componentDidMount() {
    const locationRes = await fetch(`http://localhost:9000/trails/location/${this.props.trailID}`);
    const locationJSON = await locationRes.json();

    const climateRes = await fetch(`http://localhost:9000/weather/climate/${locationJSON.lat}/${locationJSON.lon}`);
    const climateJSON = await climateRes.json();

    this.setState({climate: climateJSON.climate});
  }

  render() {
    if (this.state === null || this.state.climate === null) {
      return <LoadingSpinner/>;
    }

    return (
      <HighchartsReact
        highcharts={Highcharts}
        options={{
          chart: {
            backgroundColor: 'transparent'
          },
          credits: {
            enabled: false
          },
          title: {
            text: ''
          },
          xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
          },
          yAxis: [
            {
              title: {
                text: 'Temperature'
              },
              labels: {
                format: '{value}°C'
              }
            },
            {
              title: {
                text: 'Precipitation'
              },
              labels: {
                format: '{value}mm',
              },
              opposite: true
            }
          ],
          plotOptions: {
            column: {
              stacking: 'normal'
            }
          },
          series: [
            {
              name: 'Precipitation',
              type: 'column',
              yAxis: 1,
              data: this.state.climate.map(month => month.prcp),
              color: 'lightblue',
              tooltip: {
                valueSuffix: 'mm'
              }
            },
            {
              name: 'Temperature',
              type: 'polygon',
              data: this.state.climate.map(month => {
                return {x: month.month - 1, y: month.tmax};
              }).concat(this.state.climate.map(month => {
                return {x: month.month - 1, y: month.tmin};
              }).reverse()),
              color: 'rgba(255, 0, 0, 0.2)'
            },
            {
              type: 'spline',
              data: this.state.climate.map(month => month.tavg),
              color: 'red',
              tooltip: {
                valueSuffix: '°C'
              },
              marker: {
                symbol: 'circle'
              },
              linkedTo: ':previous'
            }
          ]
        }}
      />
    );
  }
}

export default ClimateOverviewChart;
