require('dotenv').config();

var commandLineArgs = require('command-line-args');
const options = commandLineArgs([
  {name: 'region', alias: 'r', type: String},
  {name: 'query', alias: 'q', type: String}
]);

var request = require('request-promise');
var $ = require('cheerio');
var csvParse = require('csv-parse');

var { Pool } = require('pg');
var pool = new Pool();

if (options.query) {
  request.get(`https://www.trailforks.com/search/regions/?search=${options.query}`).then(html => {    
    $('.table1 > tbody > tr', html).each(function() {
      const nameHTML = $(this).find('td:nth-child(2) > a');

      console.log(`${nameHTML.text()} | ID: ${nameHTML.attr('href').split('/')[4]}`);
    });

    process.exit();
  });
}

if (options.region) {
  let queryRegion;
  let numAllReports = 0;

  function addReports(reports, trail, callback) {
    let query = 'INSERT INTO report (date, trail_id, open, ideal) VALUES ';
    let values = [];
    let n = 1;

    for (let report of reports) {
      let ampm, date, open, ideal;

      try {
        ampm = report.time.substr(-2);
        date = new Date(report.date.replace(/\-/g, '/') + ' ' + report.time.replace(ampm, ' ' + ampm));

        open = !report.status.startsWith('Closed');

        ideal = report.status.startsWith('All Clear');
      } catch {
        continue;
      }

      values.push(date, trail, open, ideal);

      query += `($${n}, $${n + 1}, $${n + 2}, $${n + 3}), `;

      n += 4;
    }

    if (n === 1) {
      callback();
    } else {
      query = query.substr(0, query.lastIndexOf(', ')) + ';';

      pool.query(query, values, err => {
        if (err) {
          console.log(err.stack);
        } else {
          numAllReports += (n - 1) / 4;
          callback();
        }
      });
    }
  }

  let numAllTrails = 0;

  let regionIDs = {};

  function addTrail(name, region, callback) {
    pool.query(`INSERT INTO trail (name, region_id) VALUES ($1, $2) RETURNING id;`, [name, region], (err, res) => {
      if (err) {
        console.log(err.stack);
      } else {
        ++numAllTrails;
        callback(res.rows[0].id);
      }
    });
  }

  function waitForRegionAdded(region) {
    return new Promise(resolve => {
      function wait() {
        if (regionIDs[region] === -1) {
          setTimeout(wait, 100);
        } else {
          resolve(regionIDs[region]);
        }
      }
      
      wait();
    });
  }

  function addRegion(name, callback) {
    if (name in regionIDs) {
      waitForRegionAdded(name).then(callback);
    } else {
      regionIDs[name] = -1;

      pool.query(`INSERT INTO region (name) VALUES ($1) RETURNING id;`, [name], (err, res) => {
        if (err) {
          console.log(err.stack);
        } else {
          regionIDs[name] = res.rows[0].id;
          callback(res.rows[0].id);
        }
      });
    }
  }

  function addTrailsForPage(pageHTML, callback) {
    let trails = [];

    $('#trails_table > tbody > tr', pageHTML).each(function() {
      const nameHTML = $(this).find('td:nth-child(2) > a');
      const name = nameHTML.text();
      const url = nameHTML.attr('href');

      let region = $(this).find('td:nth-child(4) > a').text();

      if (region.length === 0) {
        region = queryRegion;
      }

      trails.push([name, url, region]);
    });

    let trailsComplete = 0;

    for (let [name, url, region] of trails) {
      addRegion(region, regionID => {
        function trailsCompletionCallback() {
          ++trailsComplete;
    
          if (trailsComplete === trails.length) {
            callback();
          }
        }

        addTrail(name, regionID, trailID => {
          request.get(url + 'reports/csv/').then(data => {
            csvParse(data, {
              columns: true,
              relax_column_count: true,
              skip_lines_with_error: true
            }, (err, reports) => {
              if (err) {
                console.log(err.stack);
              } else {        
                addReports(reports, trailID, trailsCompletionCallback);
              }
            });
          }).catch(err => {
            if (err.statusCode === 503) {
              process.stdout.clearLine();
              process.stdout.cursorTo(0);
              console.log('Trailforks overloaded. Try a smaller region.');
              process.exit();
            }
          });
        });
      });
    }
  }

  request.get(`https://www.trailforks.com/region/${options.region}/trails/?activitytype=1`).then(html => {
    if ($('title', html).text() === 'Error') {
      console.log('Invalid region.');
      process.exit();
    }

    pool.connect();

    queryRegion = $('.breadcrumb > ul > li:last-child > a > span', html).text();

    const numPages = Math.ceil(parseInt($('#contentTotal', html).text().replace(/\,/, '')) / 100);

    let pagesComplete = 0;

    let counterTimer = setInterval(function() {
      process.stdout.clearLine();
      process.stdout.cursorTo(0);
      process.stdout.write(
        `Added ${numAllTrails} trail${numAllTrails.length === 1 ? '' : 's'} `
        + `${numAllReports} report${numAllTrails.length === 1 ? '' : 's'}, `
        + `and ${Object.keys(regionIDs).length} region${Object.keys(regionIDs).length === 1 ? '' : 's'}.`
      );
    }, 1000);

    function completionCallback() {
      ++pagesComplete;

      if (pagesComplete === numPages) {
        clearInterval(counterTimer);

        process.stdout.clearLine();
        process.stdout.cursorTo(0);
        console.log(
          `Added ${numAllTrails} trail${numAllTrails.length === 1 ? '' : 's'}, `
          + `${numAllReports} report${numAllTrails.length === 1 ? '' : 's'}, `
          + `and ${Object.keys(regionIDs).length} region${Object.keys(regionIDs).length === 1 ? '' : 's'}. `
          + 'Complete.'
        );

        pool.end();
        process.exit();
      }
    }

    addTrailsForPage(html, completionCallback);

    for (let page = 2; page <= numPages; ++page) {
      request.get(`https://www.trailforks.com/region/${options.region}/trails/?activitytype=1&page=${page}`).then(html => {
        addTrailsForPage(html, completionCallback);
      }).catch(err => {
        if (err.statusCode === 503) {
          process.stdout.clearLine();
          process.stdout.cursorTo(0);
          console.log('Trailforks overloaded. Try a smaller region.');
          process.exit();
        }
      });
    }
  }).catch(err => {
    if (err.statusCode === 503) {
      process.stdout.clearLine();
      process.stdout.cursorTo(0);
      console.log('Trailforks overloaded. Try a smaller region.');
      process.exit();
    }
  });
}