require('dotenv').config();

var { Client } = require('pg');
var client = new Client();
client.connect();

var fs = require('fs');

const query = fs.readFileSync('./db/init.sql').toString();

client.query(query, err => {
  if (err) {
    console.log('Database initialization failed');
    console.log(err.stack);
  } else {
    console.log('Database initialization complete');
  }

  client.end();
  process.exit();
});