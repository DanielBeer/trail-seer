CREATE TABLE region (
  id SERIAL PRIMARY KEY,
  name VARCHAR NOT NULL,
  retrieved TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now()
);

CREATE TABLE trail (
  id SERIAL PRIMARY KEY,
  name VARCHAR NOT NULL,
  region_id INT NOT NULL,
  retrieved TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now()
);

CREATE TABLE report (
  id SERIAL PRIMARY KEY,
  date TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  trail_id INT NOT NULL,
  open BOOLEAN NOT NULL,
  ideal BOOLEAN,
  retrieved TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now()
);

CREATE TABLE filter (
  id SERIAL PRIMARY KEY,
  created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
  num_options INT NOT NULL
);

CREATE TABLE filter_option_type (
  id SERIAL PRIMARY KEY,
  option_type VARCHAR NOT NULL
);

INSERT INTO filter_option_type (option_type) VALUES
  ('name'),
  ('region');

CREATE TABLE filter_option (
  option_type_id INT NOT NULL REFERENCES filter_option_type(id),
  option_value VARCHAR NOT NULL,
  filter_id INT NOT NULL
);