function getFilter(pool, id) {
  return pool.query(
  `SELECT o.option_value, t.option_type
    FROM filter_option o INNER JOIN filter_option_type t ON o.option_type_id = t.id
    WHERE o.filter_id = $1`,
  [id]);
}

module.exports.getFilter = getFilter;