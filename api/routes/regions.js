require('dotenv').config();

var express = require('express');
var router = express.Router();

var { Pool } = require('pg');
var pool = new Pool();
pool.connect();

router.get('', (req, res) => {
  pool.query(
  `SELECT t1.*, t2.num_reports FROM
    (SELECT r.*, count(*) AS num_trails
    FROM region r INNER JOIN trail t ON r.id = t.region_id
    GROUP BY r.id) AS t1
    INNER JOIN
    (SELECT r.*, count(*) AS num_reports
    FROM region r INNER JOIN trail t ON r.id = t.region_id INNER JOIN report rr ON t.id = rr.trail_id
    GROUP BY r.id) AS t2
    ON t1.id = t2.id`,
  (err, qres) => {
    if (err) {
      console.error(err.stack);
      res.status(500).send();
    } else {
      res.json(qres.rows);
    }
  });
});

router.get('/:region', (req, res) => {
  pool.query(
  `SELECT t1.*, t2.num_reports
    FROM (SELECT r.*, count(*) AS num_trails
    FROM region r INNER JOIN trail t ON r.id = t.region_id
    GROUP BY r.id) AS t1
    INNER JOIN
    (SELECT r.*, count(*) AS num_reports
    FROM region r INNER JOIN trail t ON r.id = t.region_id INNER JOIN report rr ON t.id = rr.trail_id
    GROUP BY r.id) AS t2
    ON t1.id = t2.id
    WHERE t.id = $1`,
  [parseInt(req.params.region)], (err, qres) => {
    if (err) {
      console.error(err.stack);
      res.status(500).send();
    } else {
      res.json(qres.rows[0]);
    }
  });
});

router.get('/:region/trails', (req, res) => {
  pool.query(
  `SELECT *
    FROM trail
    WHERE region_id = $1`,
  [parseInt(req.params.query)], (err, qres) => {
    if (err) {
      console.error(err.stack);
      res.status(500).send();
    } else {
      res.json(qres.rows);
    }
  });
});

router.get('/:region/reports', (req, res) => {
  pool.query(
  `SELECT r.*, r.open::int + r.ideal::int AS score
    FROM report r INNER JOIN trail t ON r.trail_id = t.id
    WHERE t.region_id = $1`,
  [parseInt(req.params.query)], (err, qres) => {
    if (err) {
      console.error(err.stack);
      res.status(500).send();
    } else {
      res.json(qres.rows);
    }
  });
});

module.exports = router;
