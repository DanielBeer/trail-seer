var express = require('express');
var request = require('request-promise');

var router = express.Router();

router.get('/:lat/:lon/:start/:end', async (req, res) => {
  const weather = await request.get({
    uri: 'https://api.meteostat.net/v2/point/daily'
      + `?lat=${req.params.lat}`
      + `&lon=${req.params.lon}`
      + `&start=${req.params.start}`
      + `&end=${req.params.end}`,
    headers: {
      'x-api-key': process.env.METEOSTATAPIKEY
    }
  });
  res.send({weather: JSON.parse(weather).data});
});

router.get('/climate/:lat/:lon', async (req, res) => {
  const climate = await request.get({
    uri: `https://api.meteostat.net/v2/point/climate?lat=${req.params.lat}&lon=${req.params.lon}`,
    headers: {
      'x-api-key': process.env.METEOSTATAPIKEY
    }
  });
  res.send({climate: JSON.parse(climate).data});
});

module.exports = router;
