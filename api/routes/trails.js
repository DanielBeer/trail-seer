require('dotenv').config();

var express = require('express');
var router = express.Router();

var { Pool } = require('pg');
var pool = new Pool();
pool.connect();

const { getFilter } = require('./utils');

router.get('', (req, res) => {
  let query = `
  SELECT * FROM (
    SELECT t.*, rr.name AS region, count(*) AS num_reports
      FROM trail t INNER JOIN report r ON t.id = r.trail_id INNER JOIN region rr ON t.region_id = rr.id
      GROUP BY t.id, rr.name
      ORDER by t.name
  ) AS _`;

  const filterID = req.query.filterId;

  if (filterID) {
    getFilter(pool, filterID).then(qres => {
      let values = [];

      if (qres.rows.length > 0) {
        query += ' WHERE';

        for (let i = 0; i < qres.rows.length; ++i) {
          query += ` position($${i + 1} IN LOWER(${qres.rows[i].option_type})) > 0${i === qres.rows.length - 1 ? '' : ' AND'}`;
          values.push(qres.rows[i].option_value);
        }
      }

      pool.query(query, values, (err, qres) => {
        if (err) {
          console.error(err.stack);
          res.status(500).send();
        } else {
          res.json(qres.rows);
        }
      });
    }).catch(err => {
      console.log(err.stack);
      res.status(500).send();
    });
  } else {
    pool.query(query, (err, qres) => {
      if (err) {
        console.error(err.stack);
        res.status(500).send();
      } else {
        res.json(qres.rows);
      }
    });
  }
});

router.get('/:trail', (req, res) => {
  pool.query(
  `SELECT t.*, rr.name AS region, count(*) AS num_reports
    FROM trail t INNER JOIN report r ON t.id = r.trail_id INNER JOIN region rr ON t.region_id = rr.id
    GROUP BY t.id, rr.name
    WHERE t.id = $1`,
  [parseInt(req.params.trail)], (err, qres) => {
    if (err) {
      console.error(err.stack);
      res.status(500).send();
    } else {
      res.json(qres.rows[0]);
    }
  });
});

router.get('/:trail/reports', (req, res) => {
  pool.query(
  `SELECT *, open::int + ideal::int AS score
    FROM report
    WHERE trail_id = $1
    ORDER BY date`,
  [req.params.trail], (err, qres) => {
    if (err) {
      console.error(err.stack);
      res.status(500).send();
    } else {
      res.json(qres.rows);
    }
  });
});

module.exports = router;
