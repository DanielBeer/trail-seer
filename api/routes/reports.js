require('dotenv').config();

var express = require('express');
var router = express.Router();

var { Pool } = require('pg');
var pool = new Pool();
pool.connect();

router.get('', (req, res) => {
  pool.query(
  `SELECT *, open::int + ideal::int AS score
    FROM report`,
  [req.params.trail], (err, qres) => {
    if (err) {
      console.error(err.stack);
      res.status(500).send();
    } else {
      res.json(qres.rows);
    }
  });
});

router.get('/:report', (req, res) => {
  pool.query(
  `SELECT *, open::int + ideal::int AS score
    FROM report
    WHERE id = $1`,
  [parseInt(req.params.report)], (err, qres) => {
    if (err) {
      console.error(err.stack);
      res.status(500).send();
    } else {
      res.json(qres.rows[0]);
    }
  });
});

module.exports = router;
