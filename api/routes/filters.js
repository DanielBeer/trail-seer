require('dotenv').config();

var express = require('express');
var router = express.Router();

var { Pool } = require('pg');
var pool = new Pool();
pool.connect();

var { getFilter } = require('./utils.js');

let filterOptions = {};

pool.query(
`SELECT *
  FROM filter_option_type`,
(err, res) => {
  if (err) {
    console.error(err.stack);
  } else {
    for (let row of res.rows) {
      filterOptions[row.option_type] = row.id;
    }
  }
});

router.post('', (req, res) => {
  const filters = req.body;

  let optionIDs = [];
  let options = [];

  for (let option in filterOptions) {
    if (option in filters) {
      optionIDs.push(filterOptions[option]);
      options.push(filters[option]);
    }
  }
  
  pool.query(
  `INSERT INTO filter (num_options) VALUES
    ($1)
    RETURNING id`,
  [options.length], (err, qres) => {
    if (err) {
      console.error(err.stack);
    } else {
      const filterID = qres.rows[0].id;

      let query = 'INSERT INTO filter_option (option_type_id, option_value, filter_id) VALUES';

      let values = [];

      for (let i = 0; i < options.length; ++i) {
        query += ` ($${3 * i + 1}, $${3 * i + 2}, $${3 * i + 3})${i === options.length - 1 ? '' : ','}`;
        values.push(optionIDs[i], options[i], filterID);
      }

      pool.query(query, values, err => {
        if (err) {
          console.error(err.stack);
          res.status(500).send();
        } else {
          res.status(201).location(`/filters/${filterID}`).send();
        }
      });
    }
  });
});

router.get('/:id', (req, res) => {
  getFilter(pool, req.params.id).then(qres => {
    res.json(qres.rows);
  }).catch(err => {
    console.log(err.stack);
    res.status(500).send();
  });
});

module.exports = router;
